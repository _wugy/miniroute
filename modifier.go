package miniroute

// Modifier can change the request and the response before the route handler is called.
// The execution path is linear: Modifier1 -> Modifier2 -> RouteHandler.
// Create a new instance by using NewModifier().
type Modifier struct {
	sort int
	fn   HandlerFunc
}

// NewModifier returns a new Modifier instance.
func NewModifier(sort int, fn HandlerFunc) Modifier {
	return Modifier{
		sort: sort,
		fn:   fn,
	}
}

//----------------------------------------------------------------------------------------------------------------------

// Modifiers is a slice of Modifier implementing sort.Interface.
type Modifiers []Modifier

// See sort.Interface Len().
func (slice Modifiers) Len() int {
	return len(slice)
}

// See sort.Interface Less().
func (slice Modifiers) Less(i, j int) bool {
	return slice[i].sort < slice[j].sort
}

// See sort.Interface Swap().
func (slice Modifiers) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}
